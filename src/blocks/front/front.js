var offset = 0;
//var offsetShadow = 100;
var cylinder = $('.front__cylinder');

setInterval(function() {
  var activeWord = $('.front__cylinder-item--active');
  offset -= 46;
  activeWord.removeClass('front__cylinder-item--active').clone().appendTo('.front__cylinder-content');
  activeWord.next('.front__cylinder-item').addClass('front__cylinder-item--active');
  cylinder.css('transform', 'translateY(' + offset + 'px)');
//  offsetShadow += (-offset);
  $('.front__cylinder-shadow-top').css('top', -(offset) - 100);
}, 2000);


var titleOffset = 0;
var frontHeader = $('.front__header');
var frontTitle = $('.front__header-title:first-child');

setInterval(function() {
  titleOffset -= 1;
  frontHeader.css('transform', 'translateX(' + titleOffset + 'px)')
}, 7);

setInterval(function() {
  frontTitle.clone().appendTo(frontHeader);
//  $('.front__header').append($('.front__title').clone());
}, 10000)
