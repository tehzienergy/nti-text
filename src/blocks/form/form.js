const db = firebase.firestore();

$('.callback .btn').click(function (e) {
  e.preventDefault();
  $(this).addClass('btn--done')
  $(this).text('Отправлено!');
  var username = $('input[name="username"]').val();
  var phone = $('input[name="phone"]').val();
  
  db.collection('basic_answer').add({
    name: username,
    phone: phone
  });
  
  $('.test').fadeIn('fast');
  $('html,body').animate({
    scrollTop: $('.test').offset().top
  }, 'slow');
})

$('.test .btn').click(function (e) {
  e.preventDefault();
  $(this).addClass('btn--done')
  
  var username = $('input[name="username"]').val();
  var phone = $('input[name="phone"]').val();
  
  var textarea1 = $('textarea[name="field1"]').val();
  var textarea2 = $('textarea[name="field2"]').val();
  
  db.collection('long_answer').add({
    name: username,
    phone: phone,
    field1: textarea1,
    field2: textarea2
  });
  
  $(this).text('Сохранено');
})
